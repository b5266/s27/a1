let http = require("http");

http.createServer(function(request, response) {
	// Route for returning all items
	if (request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end('Welcome to Booking System');
	}
	if (request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end('Welcome to your Profile!');
	}
	if (request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end("Here's our courses available");
	}
	if (request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end('Add a course to our resources');
	}
	if (request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end('Update a course to our resources');
	}
	if (request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end('Archive courses to our resources');
	}
}).listen(4000);

console.log('Server running at localhost:4000');